package pl.sda.singleton.grawojna;

public enum Kolor {
    KIER("♥"),
    PIK("♠"),
    KARO("♦"),
    TREFL("♣");




    private String symbol;


    Kolor(String s) {
        this.symbol=s;
    }
    public String getSymbol(){
        return symbol;
    }
}
