package pl.sda.singleton.grawojna;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class Gracz {
    private Queue<Karta> kartyWReku = new LinkedList<Karta>();
    private List<Karta> kartyNaStole = new ArrayList<Karta>();

    public Queue<Karta> getkartyWReku() {
        return kartyWReku;
    }

    public List<Karta> getKartyNaStole() {
        return kartyNaStole;
    }
}
