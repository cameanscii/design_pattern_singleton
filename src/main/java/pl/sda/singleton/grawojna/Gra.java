package pl.sda.singleton.grawojna;

public class Gra {

    Gracz graczA = new Gracz();
    Gracz graczB = new Gracz();
    Talia talia = new Talia();

    public void potasujrozdajKarty(){
        talia.stworzTalie();
        for (int i = 0; i < 26; i++) {
            graczA.getkartyWReku().add(talia.losujKarte());
            graczB.getkartyWReku().add(talia.losujKarte());
        }



        System.out.println(graczA.getkartyWReku());
        System.out.println(graczB.getkartyWReku());
        System.out.println(talia.getTaliaKart());
    }


    public void rozpocznijGre() {
        System.out.println("Rozpoczęto grę");
        while ((graczA.getkartyWReku().size() != 0 ) ||
                (graczB.getkartyWReku().size() != 0 )){

            Karta biezacaKartaA = graczA.getkartyWReku().peek();
            Karta biezacaKartaB = graczB.getkartyWReku().peek();
            System.out.println("Karta gracza A to "+graczA.getkartyWReku().peek());
            System.out.println("Karta gracza B to "+graczB.getkartyWReku().peek());



            if (biezacaKartaA.figura.getValue() > biezacaKartaB.figura.getValue()) {

                graczA.getkartyWReku().add(graczB.getkartyWReku().remove());
                System.out.println("Gracz A wygrywa rozdanie");

            } else if (biezacaKartaA.figura.getValue() < biezacaKartaB.figura.getValue()) {

                graczB.getkartyWReku().add(graczA.getkartyWReku().remove());
                System.out.println("Gracz B wygrywa rozdanie");

            } else if (biezacaKartaA.figura.getValue() == biezacaKartaB.figura.getValue()) {
                System.out.println("Wojna!");
                int index=0;
                int indicatorOfEnd=0;
                while (indicatorOfEnd==0) {
                    graczA.getKartyNaStole().add(graczA.getkartyWReku().remove());
                    graczB.getKartyNaStole().add(graczB.getkartyWReku().remove());
                    if (index % 2 == 0 && index != 0) {


                        System.out.println("Karta gracza A to "+graczA.getkartyWReku().peek());
                        System.out.println("Karta gracza B to "+graczB.getkartyWReku().peek());

                        if (graczA.getkartyWReku().peek().figura.getValue() > graczB.getkartyWReku().peek().figura.getValue()) {

                            graczA.getkartyWReku().addAll(graczB.getKartyNaStole());
                            indicatorOfEnd=1;
                            System.out.println("Gracz A wygrywa rozdanie");
                            break;

                        } else if (graczA.getkartyWReku().peek().figura.getValue() < graczB.getkartyWReku().peek().figura.getValue()) {

                            graczB.getkartyWReku().addAll(graczA.getKartyNaStole());
                            System.out.println("Gracz B wygrywa rozdanie");

                            indicatorOfEnd=1;
                            break;
                        }
                        System.out.println("Brak rozstrzygnięcia!");
                    }
                    index++;
                }
            }

        }
        System.out.println("Gra zakończona");
    }


}
