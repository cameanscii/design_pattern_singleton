package pl.sda.singleton.grawojna;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Talia{
    private List<Karta> taliaKart = new ArrayList<Karta>();

    public List<Karta> getTaliaKart() {
        return taliaKart;
    }

    Random random=new Random();


    public void stworzTalie() {
        for (Kolor kolor1 : Kolor.values()) {
            for (Figura figura1 : Figura.values()) {
                taliaKart.add(new Karta(kolor1, figura1));
            }
        }
    }


    public Karta losujKarte() {
        return taliaKart.remove(random.nextInt(taliaKart.size()));
    }

}

