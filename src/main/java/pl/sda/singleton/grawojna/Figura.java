package pl.sda.singleton.grawojna;

public enum Figura {
    AS(14),
    WALET(11),
    KROLOWA(12),
    KROL(13),
    DWOJKA(2),
    TROJKA(3),
    CZWORKA(4),
    PIATKA(5),
    SZOSTKA(6),
    SIODEMKA(7),
    OSEMKA(8),
    DZIEWIATKA(9),
    DZIESIATKA(10);

    private int value;

    Figura(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
