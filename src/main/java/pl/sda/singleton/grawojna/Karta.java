package pl.sda.singleton.grawojna;

public class Karta {
    Kolor kolor;
    Figura figura;

    public Karta(Kolor kolor, Figura figura) {
        this.kolor = kolor;
        this.figura = figura;
    }

    @Override
    public String toString() {
        String nazwa = figura.toString().substring(0, 1) + figura.toString().substring(1).toLowerCase();
        String znak = kolor.getSymbol();
        return nazwa + " " + znak;
    }


}

