package pl.sda.singleton.gratabliczkamnozenia;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SettingsReader settingsReader=new SettingsReader();
        settingsReader.open();

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int liczbaPunktów=0;

        for (int i = 0; i <MySettings.getInstance().getNumberOfGames() ; i++) {
            int firstNumber=random.nextInt(MySettings.getInstance().getRange1());
            int secondNumber=random.nextInt(MySettings.getInstance().getRange2());
            int calculationIndex=random.nextInt(3);
            char calculation= MySettings.getInstance().getCalculationType()[calculationIndex];
            int wynik=0;
            switch (calculation) {
                case '+':
                    wynik = firstNumber + secondNumber;
                    break;
                case '-':
                    wynik = firstNumber - secondNumber;
                    break;
                case '/':
                    wynik = firstNumber / secondNumber;
                    break;
                case '*':
                    wynik = firstNumber * secondNumber;
                    break;
            }

            System.out.println("Runda "+(i+1));
            System.out.printf("Ile wynosi %d%c%d ?\n", firstNumber, calculation,secondNumber );
            String odpowiedz=scanner.nextLine();

            if (wynik==Integer.parseInt(odpowiedz)){
                liczbaPunktów++;
            }

        }
        System.out.println("Liczba zdobytych punktów: "+liczbaPunktów);

    }
}
