package pl.sda.singleton.gratabliczkamnozenia;

import pl.sda.singleton.gratabliczkamnozenia.MySettings;

import java.io.*;

public class SettingsReader {


    File plik = new File("C:\\Users\\kamyk\\IdeaProjects\\design_pattern_singleton\\singletonSettings.txt");

     public void open(){

         try {

             BufferedReader reader = new BufferedReader(new FileReader(plik));
             MySettings.getInstance().setRange1(Integer.parseInt(reader.readLine()));
             MySettings.getInstance().setRange2(Integer.parseInt(reader.readLine()));
             MySettings.getInstance().setCalculationType(reader.readLine().toCharArray());
             MySettings.getInstance().setNumberOfGames(Integer.parseInt(reader.readLine()));


         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }



}
