package pl.sda.singleton.gratabliczkamnozenia;

public class MySettings {
    private int range1;
    private int range2;
    private char[] calculationType;
    private int numberOfGames;

    private static MySettings mySettings= new MySettings();
    private MySettings() {
    }

    public static MySettings getInstance(){
        return mySettings;
    }

    public int getRange1() {
        return range1;
    }

    public void setRange1(int range1) {
        this.range1 = range1;
    }

    public int getRange2() {
        return range2;
    }

    public void setRange2(int range2) {
        this.range2 = range2;
    }

    public char[] getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(char[] calculationType) {
        this.calculationType = calculationType;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }
}
